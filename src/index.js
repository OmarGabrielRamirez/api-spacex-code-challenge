const express = require ('express');
const morgan = require('morgan');
const serverApp  = express();

serverApp.set('port' , process.env.PORT || 3000);
serverApp.use(morgan('dev'));
serverApp.use(express.urlencoded({extended: false}));
serverApp.use(express.json());

serverApp.use(require('./routes/index_routes'));

serverApp.listen(serverApp.get('port'), () => {
    console.log(`------------API REST TO SPACEX ------------- PORT:${serverApp.get('port')}`);
});

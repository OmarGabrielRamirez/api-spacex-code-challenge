
const { Router } = require('express');
const fetch = require('node-fetch');
const _ = require('underscore');
const router = Router();

router.get('/api/get/all/rockets', async (request, res) => {
    const api_url = `https://api.spacexdata.com/v3/rockets`;
    const response = await fetch(api_url);
    const json = await response.json();
    res.json(json);
});

router.post('/api/get/details/rocket/:idrocket', async (request, res) =>{
    const id_rocket  = request.params.idrocket;
    const api_url = `https://api.spacexdata.com/v3/rockets/${id_rocket}`;    
    const response = await fetch(api_url);
    const json = await response.json();
    res.json(json);
});

module.exports = router;

